package pt.ipleiria.deepinsta;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;
import pt.ipleiria.deepinsta.R;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button selectImage, takePhoto, shareImage;
    private ImageView imageView;
    private TextView hashtags;

    private static Uri imageUri = null;
    private final int select_photo = 1;
    private final int take_photo = 2;
    private String encodedImage = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        selectImage = findViewById(R.id.select_image);
        takePhoto = findViewById(R.id.btnTakePhoto);
        shareImage = findViewById(R.id.share_image);
        imageView = findViewById(R.id.share_imageview);

        hashtags = findViewById(R.id.textViewHashtag);

        selectImage.setOnClickListener(this);
        takePhoto.setOnClickListener(this);
        shareImage.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.select_image:
                // Intent to gallery
                Intent in = new Intent(Intent.ACTION_PICK);
                in.setType("image/*");
                startActivityForResult(in, select_photo);// start activity for result
                break;
            case R.id.share_image:
                // share image
                shareImage(imageUri);
                break;
            case R.id.btnTakePhoto:
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, take_photo);
                break;
        }
    }

    protected void onActivityResult(int requestcode, int resultcode, Intent imagereturnintent) {
        super.onActivityResult(requestcode, resultcode, imagereturnintent);
        switch (requestcode) {
            case select_photo:
                if (resultcode == RESULT_OK) {
                    try {
                        imageUri = imagereturnintent.getData();// Get intent data

                        Bitmap bitmap = Utils.decodeUri(MainActivity.this, imageUri, 200);
                        // call decode uri method
                        // Check if bitmap is not null then set image else show toast
                        if (bitmap != null) {
                            imageView.setImageBitmap(bitmap);// Set image over bitmap
                            shareImage.setVisibility(View.VISIBLE);// Visible button if bitmap is not null
                            encodedImage = encodeImage(bitmap);

                            RequestQueue MyRequestQueue = Volley.newRequestQueue(this);

                            String url = "http://128.199.41.103/predict";
                            JSONObject data = new JSONObject();
                            try {
                                data.put("image", encodedImage);
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            JsonObjectRequest MyStringRequest = new JsonObjectRequest(Request.Method.POST, url, data, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    //This code is executed if the server responds, whether or not the response contains data.
                                    //The String 'response' contains the server's response.

                                    System.out.println("++++++++++++++ Ans" + response);

                                    try {
                                        JSONObject predictions = response.getJSONObject("predictions");
                                        Iterator<String> keys = predictions.keys();
                                        float highest = 0;
                                        String hashtag = "NONE";
                                        while(keys.hasNext()) {
                                            String key = keys.next();
                                            float value = Float.parseFloat(predictions.get(key).toString());
                                            if (value  > highest){
                                                hashtag = key;
                                                highest = value;
                                            }
                                        }
                                        hashtags.setText(hashtag);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }, new Response.ErrorListener() { //Create an error listener to handle errors appropriately.
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //This code is executed if there is an error.
                                    System.out.println("************ Err" + error.toString());
                                }
                            });

                            MyRequestQueue.add(MyStringRequest);

                        } else {
                            shareImage.setVisibility(View.GONE);
                            Toast.makeText(MainActivity.this, "Error while decoding image.", Toast.LENGTH_SHORT).show();
                        }
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Toast.makeText(MainActivity.this, "File not found.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    // Share image
    private void shareImage(Uri imagePath) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
        sharingIntent.setType("image/*");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, "Hello");
        sharingIntent.putExtra(Intent.EXTRA_STREAM, imagePath);
        startActivity(Intent.createChooser(sharingIntent, "Share Image Using"));
    }

    private String encodeImage(Bitmap bm) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.NO_WRAP);

        return encImage;
    }

//    public String getPath(Uri uri) {
//        String[] projection = {MediaStore.MediaColumns.DATA};
//        Cursor cursor = managedQuery(uri, projection, null, null, null);
//        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
//        cursor.moveToFirst();
//        return cursor.getString(column_index);
//    }

}
